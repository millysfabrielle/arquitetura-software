package br.com.arquiteturasoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArquiteturaSoftApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArquiteturaSoftApplication.class, args);
	}

}
