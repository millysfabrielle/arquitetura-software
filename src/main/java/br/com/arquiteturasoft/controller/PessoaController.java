package br.com.arquiteturasoft.controller;

import br.com.arquiteturasoft.entity.Pessoa;
import br.com.arquiteturasoft.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pessoa")
public class PessoaController {

    @Autowired
    PessoaService service;


    @GetMapping
    public ResponseEntity<?> listAll(){
        List<Pessoa> lista = service.findAll();
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?>  findById( @PathVariable long id){
        Pessoa pessoa = service.findById(id);

        HttpStatus status;
        if(pessoa != null){
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(pessoa, HttpStatus.OK);
    }

    @PostMapping
    public void save( @RequestBody Pessoa pessoa ){

    }

    @PutMapping("/{id}")
    public void update(  @PathVariable int id, @RequestBody Pessoa pessoa){

    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){

    }

}
