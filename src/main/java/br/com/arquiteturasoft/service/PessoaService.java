package br.com.arquiteturasoft.service;

import br.com.arquiteturasoft.entity.Pessoa;
import br.com.arquiteturasoft.repository.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PessoaService {

    @Autowired
    PessoaRepository repository;

    public List<Pessoa> findAll(){
        List<Pessoa> lista = repository.findAll();
        return lista;

    }

    public Pessoa findById( long id){
        Pessoa pessoa = repository.getOne(id);
        System.out.println("PESSOA");
        return pessoa;
    }

    public boolean save(Pessoa pessoa){
        return false;
    }

    public boolean delete(int id){
        return  false;
    }


}
